from django.shortcuts import render, redirect, get_object_or_404
from .models import ExpenseCategory, Account, Receipt
from .forms import ReceiptForm, CategoryForm, AccountForm
from django.contrib.auth.decorators import login_required


@login_required
def receipt_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
            "receipts": receipts,
            }
    return render(request, "receipts/list.html", context)

@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(commit=False)
            account.owner = request.user
            account.save()
        else:
            print("form isn't valid")
        return redirect("account_list")
    else:
        form = AccountForm()
    context = {
            "form": form,
            }
    return render(request, "accounts/create.html", context)
    pass

@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
        else:
            print("form isn't valid")
        return redirect("home")
    else:
        form = ReceiptForm()
        form.fields['category'].queryset = ExpenseCategory.objects.filter(owner=request.user)
        form.fields['account'].queryset = Account.objects.filter(owner=request.user)
    context = {
            "form": form,
            }
    return render(request, "receipts/create.html", context)

@login_required
def create_category(request):
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            expense_category = form.save(commit=False)
            expense_category.owner = request.user
            expense_category.save()
        else:
            print("form not valid!")
        return redirect("category_list")
    else:
        form = CategoryForm()
    context = {
            "form": form,
            }
    return render(request, "categories/create.html", context)


@login_required
def category_list(request):
    expense_categories= ExpenseCategory.objects.filter(owner=request.user)
    context = {
            "categories": expense_categories,
            }
    return render(request, "receipts/categories.html", context)

@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
            "accounts": accounts,
            }
    return render(request, "receipts/accounts.html", context)

