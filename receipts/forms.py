from django import forms
from django.forms import ModelForm
from .models import Receipt, ExpenseCategory, Account


class ReceiptForm(ModelForm):
    class Meta:
        model = Receipt
        fields = "__all__"
        exclude = ['purchaser']

class CategoryForm(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = "__all__"
        exclude = ['owner']

class AccountForm(ModelForm):
    class Meta:
        model = Account
        fields = "__all__"
        exclude= ['owner']
