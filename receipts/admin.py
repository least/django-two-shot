from django.contrib import admin
from .models import ExpenseCategory, Account, Receipt


@admin.register(ExpenseCategory)
class TodoListAdmin(admin.ModelAdmin):
    list_display = (
            "name",
            "owner",
            )


@admin.register(Account)
class TodoListAdmin(admin.ModelAdmin):
    list_display = (
            "name",
            "number",
            "owner",
            )

@admin.register(Receipt)
class TodoListAdmin(admin.ModelAdmin):
    list_display = (
            "vendor",
            "total",
            "tax",
            "purchaser",
            "category",
            "account",
            )
